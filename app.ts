import { watchForecast } from "./server/task";
import {initWebsocket} from "./server/websocket-config";
import {initServer} from "./server/server";

const timeout = 60000;

initServer()
    .then(initWebsocket)
    .then(send => setInterval(() => watchForecast(send), timeout));
