const title = 'Title';
const lang = 'en-US';
const dir: NotificationDirection = 'auto';

const checkPermission = (): Promise<void> => {
    if (Notification.permission !== 'granted') {
        return Notification.requestPermission().then(checkPermission).catch(console.error);
    } else {
        return Promise.resolve();
    }
}

const createNotification = (body: string) => {
    const timestamp = Date.now();
    const tag = Math.floor(timestamp / 1000).toString(32);
    const options = { body, lang, dir, tag, timestamp };
    const notification = new Notification(title, options);
    notification.onclick = () => window.open('https://www.idokep.hu/elorejelzes/Budapest');

    return notification;
};

const webSocketUrl = location.protocol === 'http:' ? `ws://${window.location.hostname}:80` : `wss://${window.location.hostname}:443`;

const createWebSocket = () => {
    const webSocket = new WebSocket(webSocketUrl);

    webSocket.addEventListener('error', event => {
        console.error(event);
    });

    return webSocket;
};

const init = () => {
    checkPermission().then(() => {
        const webSocket = createWebSocket();
        webSocket.addEventListener('error', event => createNotification(JSON.stringify(event)));
        webSocket.addEventListener('message', event => createNotification(JSON.stringify(event.data)));
    });
}

init();
