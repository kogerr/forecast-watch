import { RequestHandler } from './types';
import { ServerResponse } from 'http';
import { Http2ServerResponse } from 'http2';
import { fileExists, readFileContent } from './file-reader';
import { mimeTypes } from './mime-types';
import { renderLastForecast } from "./forecast-render";
import { latestEntries } from "./task";

const staticDirectory = './src/';

export const route: RequestHandler = (req, res) => {
    const cleanUrl = req.url?.split('?')[0];
    const path = (!cleanUrl|| cleanUrl === '/') ? '/' : cleanUrl;
    const method = req.method ? req.method : 'GET';

    res.statusCode = 200;

    if (method === 'GET' && fileExists(staticDirectory + path)) {
        return sendFile(res, path);
    } else {
        sendLastForecast(res);
    }
};

const sendFile = (res: ServerResponse | Http2ServerResponse, path: string): Promise<void> => {
    const type = mimeTypes.get(path.slice(path.lastIndexOf('.')));
    if (type) {
        res.setHeader('Content-Type', type);
    }

    return readFileContent(staticDirectory + path).then((content: Buffer) => {
        res.statusCode = 200;
        res.end(content, 'binary');
    }).catch((error) => {
        res.statusCode = 500;
        res.end(JSON.stringify({ error }));
    });
};

const sendLastForecast = (res: ServerResponse | Http2ServerResponse) => {
    const content = renderLastForecast(latestEntries);

    res.setHeader('Content-Type', 'text/html');
    res.statusCode = 200;
    res.end(content);
};
