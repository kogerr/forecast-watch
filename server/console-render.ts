import {ForecastEntry} from "./forecast-entry.interface";

const getMaxLengths = (allValues: string[][]) => {
    const maxLengths: [number, number, number, number, number] = [0, 0, 0, 0, 0];
    allValues.forEach(values => {
        for (let i = 0; i < values.length; i++) {
            const length = values[i].length;
            if (length > maxLengths[i]) {
                maxLengths[i] = length;
            }
        }
    });

    return maxLengths;
};

const getLine = (values: string[], maxLengths: [number, number, number, number, number]) => {
    const columns = [];
    for (let i = 0; i < 5; i++) {
        const value = (values[i] ? values[i] : '');
        columns.push(value.padEnd(maxLengths[i]));
    }

    return '│ ' + columns.join(' │ ') + ' │';
};

export const renderAsciiTable = (forecastEntries: ForecastEntry[]) => {
    const timeTop = '┌───────┐';
    const time = '│ ' + (new Date()).toTimeString().substring(0, 5) + ' │';
    const allValues = forecastEntries.map(entry => Object.values(entry) as string[]);
    const maxLengths = getMaxLengths(allValues);
    const top = '├─' + maxLengths.map(length => '─'.repeat(length)).join('─┬─') + '─┐';
    const separator = '├─' + maxLengths.map(length => '─'.repeat(length)).join('─┼─') + '─┤';
    const lines = allValues.map(values => getLine(values, maxLengths)).join('\n' + separator + '\n');
    const bottom = '└─' + maxLengths.map(length => '─'.repeat(length)).join('─┴─') + '─┘';

    return [ timeTop, time, top, lines, bottom ].join('\n');
};
