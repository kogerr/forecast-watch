import { get, RequestOptions } from 'https';
import { IncomingMessage } from "http";
import { ForecastEntry } from "./forecast-entry.interface";
import { renderAsciiTable } from "./console-render";

const host = 'www.idokep.hu';
const path = '/elorejelzes/Budapest';
const options: RequestOptions = { host, path };
const greenCode = '\x1b[32m';

let lastPublished = '';
export let latestEntries: ForecastEntry[] = [];

export const watchForecast = (send: (data: string) => void) => {
    download()
        .then(page => sendUpdateOnChange(page, send))
        .catch(console.error);
};

const download = () => new Promise<string>((resolve, reject) => {
    get(options, (res: IncomingMessage) => {
        let data = '';

        if (res.statusCode !== 200) {
            reject(res.headers);
        }

        res.on('error', reject);
        res.on('data', (chunk) => data += chunk);
        res.on('end', () => resolve(data));
    });
});

const sendUpdateOnChange = (page: string, send: (data: string) => void) => {
    const published = getPublished(page);
    const content = getContent(page);
    const entries = getEntries(content);

    if (published !== lastPublished) {
        send(`New forecast published at ${published}`);
        console.log(greenCode, renderAsciiTable(entries));
    } else if (JSON.stringify(entries) !== JSON.stringify(latestEntries)) {
        send(getDiffMessage(latestEntries, entries));
        console.log(greenCode, renderAsciiTable(entries));
    }

    lastPublished = published;
    latestEntries = entries;
};

// @ts-ignore
const getPublished = (text: string) => text.match(/class="kiadta".+?(\d{1,2}:\d{2})/)[1];

// @ts-ignore
const getContent = (text: string) => text.match(/class="tizenket">\s+(.+)<div class="kiadta"/s)[1];

const getEntries = (content: string) => {
    const parts = content.split('<div class="oszlop">').filter(part => part !== '');

    return parts.map(extractValues);
};

const extractValues = (part: string): ForecastEntry => {
    // @ts-ignore
    const hour: string = part.match(/<div class="ora">(.+)<\/div>/)[1];
    // @ts-ignore
    const temperature = part.match(/<div class="hoerzet \w*">(.+)<\/div>/)[1];
    // @ts-ignore
    const percent = part.match(/<div class="percent">(.*)<\/div>/)[1];
    // @ts-ignore
    const weather = part.match(/<strong class="felhos-text">(.*)<\/strong>/)[1]
    const values: ForecastEntry = { hour, temperature, percent, weather };
    const precipitationMatch = part.match(/<strong class="csapadek-text">(.*)<\/strong>/);
    if (precipitationMatch && precipitationMatch.length > 1) {
        values.precip = precipitationMatch[1];
    }

    return values;
};

const getDiffMessage = (oldEntries: ForecastEntry[], newEntries: ForecastEntry[]) => {
    const entryCount = Math.max(oldEntries.length, newEntries.length);
    const diffs: string[] = [];

    for (let i = 0; i < entryCount; i++) {
        if (JSON.stringify(oldEntries[i]) !== JSON.stringify(newEntries[i])) {
            diffs.push(getDiff(oldEntries[i], newEntries[i]));
        }
    }

    return diffs.join('; ');
};

const getDiff = (oldEntry: ForecastEntry, newEntry: ForecastEntry) => {
    const oldValues = Object.values(oldEntry);
    const newValues = Object.values(newEntry);
    const diffOld = [];
    const diffNew = [];

    for (let i = 0; i < 5; i++) {
        if (oldValues[i] !== newValues[i]) {
            diffOld.push(oldValues[i]);
            diffNew.push(newValues[i]);
        }
    }

    return oldEntry.hour + ': ' + diffOld.join(' ') + ' => ' + diffNew.join(' ');
};
