import { Server } from 'http';
import * as ws from 'websocket';

export const initWebsocket = (httpServer: Server) => {
    // @ts-ignore
    const wsServer = new ws.server({httpServer});
    const send = (data: string) => wsServer.broadcastUTF(data);

    // @ts-ignore
    wsServer.on('request', (request: ws.request) => {
        const connection = request.accept(undefined, request.origin);
        console.log('connected host:', request.remoteAddress);

        // @ts-ignore
        connection.on('message', console.log);

        connection.on('close', (code: number) => {
            console.log('Connection closed with code: ' + code);
        });
    });

    return send;
};
