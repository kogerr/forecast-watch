export interface ForecastEntry {
    hour: string;
    temperature: string;
    percent: string;
    weather: string;
    precip?: string
}
