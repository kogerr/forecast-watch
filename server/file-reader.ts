import { readFile, statSync } from 'fs';

export const fileExists = (path: string) => {
    try {
        return statSync(path).isFile();
    } catch (error) {
        return false;
    }
};

export const readFileContent = (path: string) => {
    return new Promise<Buffer>((resolve, reject) => {
        readFile(path, {}, (error, content: Buffer) => {
            if (error) {
                reject(error);
            } else {
                resolve(content);
            }
        });
    });
};
