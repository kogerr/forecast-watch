import { ForecastEntry } from "./forecast-entry.interface";

const templateStart = `
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forecast</title>
    <link rel="stylesheet" href="style.css" />
</head>
<body>
    <table border=1 frame=void rules=rows>
        <tbody>
`;
const templateEnd = `
        </tbody>
    </table>
</body>
<script src="script.js"></script>
</html>
`;

const renderTableRow = (entry: ForecastEntry) => {
    const values = Object.values(entry);
    const columns = [];
    for (let i = 0; i < 5; i++) {
        columns.push(values[i] ? `<td>${values[i]}</td>` : '<td></td>');
    }

    return '<tr>' + columns.join('') + '</tr>';
}

export const renderLastForecast = (forecastEntries: ForecastEntry[]) =>
    templateStart + forecastEntries.map(renderTableRow).join('') + templateEnd;
